﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;

namespace OvermobileTest.Game
{

    /// <summary>
    /// Глобальное состояние игры, синхронизируемое между клиентом и сервером.
    /// 
    /// Используется для синхронизации количества набранных очков, состояния игры (завершилась ли она) и текущего времени
    /// </summary>
    public class SynchronizedGameState : NetworkBehaviour, IGameState, IServerTime
    {
        [Tooltip("Период, с которым данные о времени отправляются клиентам.")]
        public float timeSyncPeriod;

        [SyncVar(hook = "OnScoreChanged")]
        private int score;

        [SyncVar(hook = "OnGameOverChanged")]
        private bool gameOver;

        /// <summary>
        /// Текущий игровой счёт.
        /// </summary>
        public int Score
        {
            get
            {
                return score;
            }
        }

        /// <summary>
        /// Если true - пользователь проиграл.
        /// </summary>
        public bool GameOver
        {
            get
            {
                return gameOver;
            }
        }

        /// <summary>
        /// Разница между временем на сервере и на клиенте (fixedTime)
        /// </summary>
        public float FixedTimeCorrection
        {
            get; private set;
        }

        public float ServerFixedTime
        {
            get
            {
                return Time.fixedTime + FixedTimeCorrection;
            }
        }

        public event EventHandler<ScoreChangedEventArgs> ScoreChanged;

        public event EventHandler<GameOverChangedEventArgs> GameOverChanged;

        public event EventHandler<FixedTimeCorrectionChangedEventArgs> FixedTimeCorrectionChanged;

        [Server]
        public void OnBallCaught()
        {
            ++score;

            OnScoreChanged(score);
        }

        [Server]
        public void OnBallMissed()
        {
            if (!gameOver)
            {
                gameOver = true;

                OnGameOverChanged(true);
            }
        }

        public void Start()
        {
            if (isServer)
            {
                StartCoroutine(DoUpdateTime());
            }
        }

        /// <summary>
        /// Отправляет текущее время на сервер с заданной переодичностью
        /// </summary>
        private IEnumerator DoUpdateTime()
        {
            while (true)
            {
                Rpc_SetServerTime(Time.fixedTime);
                yield return new WaitForSeconds(timeSyncPeriod);
            }
        }

        /// <summary>
        /// Server time update
        /// </summary>
        /// <param name="serverFixedTime"></param>
        [ClientRpc]
        private void Rpc_SetServerTime(float serverFixedTime)
        {
            float newFixedTimeCorrection = serverFixedTime - Time.fixedTime;

            // Меняем коррекцию только в сторону увеличения
            if (newFixedTimeCorrection > FixedTimeCorrection) {
                FixedTimeCorrection = newFixedTimeCorrection;

                OnFixedTimeCorrectionChanged(FixedTimeCorrection);
            }
        }

        private void OnFixedTimeCorrectionChanged(float fixedTimeCorrection)
        {
            EventHandler<FixedTimeCorrectionChangedEventArgs> temp = FixedTimeCorrectionChanged;
            if (temp != null)
            {
                temp(this, new FixedTimeCorrectionChangedEventArgs(fixedTimeCorrection));
            }
        }

        private void OnScoreChanged(int score)
        {
            EventHandler<ScoreChangedEventArgs> temp = ScoreChanged;
            if (temp != null)
            {
                temp(this, new ScoreChangedEventArgs(score));
            }
        }

        private void OnGameOverChanged(bool gameOver)
        {
            EventHandler<GameOverChangedEventArgs> temp = GameOverChanged;
            if (temp != null)
            {
                temp(this, new GameOverChangedEventArgs(gameOver));
            }
        }
    }
}