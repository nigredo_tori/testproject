﻿using System;

namespace OvermobileTest.Game
{
    public interface IServerTime
    {
        float FixedTimeCorrection { get; }
        float ServerFixedTime { get; }

        event EventHandler<FixedTimeCorrectionChangedEventArgs> FixedTimeCorrectionChanged;
    }

    public class FixedTimeCorrectionChangedEventArgs : EventArgs
    {
        public float FixedTimeCorrection { get; private set; }

        public FixedTimeCorrectionChangedEventArgs(float fixedTimeCorrection)
        {
            this.FixedTimeCorrection = fixedTimeCorrection;
        }
    }
}