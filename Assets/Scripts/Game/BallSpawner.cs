﻿using UnityEngine;
using OvermobileTest.Util;
using UnityEngine.Networking;

namespace OvermobileTest.Game
{
    /// <summary>
    /// Создаёт падающие шары.
    /// 
    /// Содержит логику зависимости скорости и количества шаров от времени.
    /// </summary>
    public class BallSpawner : MonoBehaviour
    {
        #region Поля для изменения в инспекторе
        [Tooltip("Зависимость скорости шаров от прошедшего времени")]
        public ExponentialCurve ballVelocityFromTime;

        [Tooltip("Зависимость промежутка времени между шарами от прошедшего времени")]
        public ExponentialCurve ballIntervalFromTime;

        [Tooltip("Интервал времени между изменениями скорости")]
        public float stepInterval;

        [Tooltip("Разброс создаваемых шаров по горизонтали")]
        public float spawnRadius;

        [Tooltip("Префаб создаваемого шара")]
        public BallLifetime ballPrefab;
        #endregion

        /// <summary>
        /// Время начала работы (fixed)
        /// </summary>
        private float startTime;

        /// <summary>
        /// Время создания следующего шара (fixed)
        /// </summary>
        private float nextSpawn;

        /// <summary>
        /// Объект-родитель для созданных шаров (чтобы не засорять редактор)
        /// </summary>
        private Transform createdBallsFolder;

        /// <summary>
        /// Вызывается каждый раз при создании шара
        /// </summary>
        public event System.Action<BallLifetime> BallSpawned;

        #region Обработчики событий Unity
        public void Start()
        {
            startTime = Time.fixedTime;
            nextSpawn = float.NegativeInfinity;

            createdBallsFolder = new GameObject("Created Balls").transform;
            createdBallsFolder.parent = transform;
            createdBallsFolder.localPosition = Vector3.zero;
        }

        public void FixedUpdate()
        {
            if (nextSpawn < Time.fixedTime)
            {
                // Пора создать ещё один шар
                SpawnBall();

                // Задаём время создания следующего шара
                float ballInterval = ballIntervalFromTime.GetValue(GetEffectiveTime());
                nextSpawn = Time.fixedTime + ballInterval;
            }
        }

        /// <summary>
        /// Создаём новый шар
        /// </summary>
        private void SpawnBall()
        {
            float effectiveTime = GetEffectiveTime();
            float ballVy = ballVelocityFromTime.GetValue(effectiveTime);
            float x = transform.position.x + Random.Range(-spawnRadius, spawnRadius) + createdBallsFolder.position.x;
            float y = transform.position.y;

            BallLifetime ball = Instantiate(ballPrefab);
            ball.transform.parent = createdBallsFolder;

            var ballMovement = ball.GetComponent<BallMovement>();
            ballMovement.Init(x, y, ballVy);

            OnBallSpawned(ball);
        }
        #endregion

        #region Вспомогательные методы

        /// <summary>
        /// Текущее эффективное время для рассчёта скорости и количества шаров.
        /// 
        /// Меняется скачками (с шагом stepInterval)
        /// </summary>
        /// <returns></returns>
        private float GetEffectiveTime()
        {
            float t = Time.fixedTime - startTime;
            return t - t % stepInterval;
        }

        private void OnBallSpawned(BallLifetime ball)
        {
            if (BallSpawned != null)
            {
                BallSpawned(ball);
            }
        }

        #endregion
    }
}