﻿using UnityEngine;
using OvermobileTest.UI;
using UnityEngine.Networking;

namespace OvermobileTest.Game
{
    /// <summary>
    /// Связывает между собой игровые подсистемы
    /// </summary>
    public class GameManager : NetworkBehaviour
    {
        [Tooltip("Пользовательский интерфейс конца уровня")]
        public GameOverScreen gameOverScreen;

        [Tooltip("Префаб дощечки")]
        public GameObject padPrefab;

        [Tooltip("Префаб состояния игры")]
        public SynchronizedGameState gameStatePrefab;

        private SynchronizedGameState gameState;
        
        void Start()
        {
            gameOverScreen.gameObject.SetActive(false);

            var spawner = FindObjectOfType<BallSpawner>();
            var scoreUI = gameObject.AddComponent<ScoreUI>();

            if (isServer)
            {
                spawner.BallSpawned += Spawner_BallSpawned;

                GameObject pad = Instantiate(padPrefab);
                NetworkServer.Spawn(pad);

                gameState = Instantiate(gameStatePrefab);
                NetworkServer.Spawn(gameState.gameObject);
            }
            else
            {
                spawner.enabled = false;
                gameState = FindObjectOfType<SynchronizedGameState>();
            }

            scoreUI.Init(gameState);
            gameState.GameOverChanged += GameState_GameOverChanged;
        }

        private void GameState_GameOverChanged(object sender, GameOverChangedEventArgs e)
        {
            gameOverScreen.Show();
        }

        private void Spawner_BallSpawned(BallLifetime ball)
        {
            NetworkServer.Spawn(ball.gameObject);
            ball.WasCaught += _ => gameState.OnBallCaught();
            ball.Missed += _ => gameState.OnBallMissed();
        }
    }
}