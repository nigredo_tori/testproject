﻿using System;

namespace OvermobileTest.Game
{
    public interface IGameState
    {
        bool GameOver { get; }
        int Score { get; }

        event EventHandler<GameOverChangedEventArgs> GameOverChanged;
        event EventHandler<ScoreChangedEventArgs> ScoreChanged;
    }

    public class GameOverChangedEventArgs : EventArgs
    {
        public bool GameOver { get; private set; }

        public GameOverChangedEventArgs(bool gameOver)
        {
            this.GameOver = gameOver;
        }
    }

    public class ScoreChangedEventArgs : EventArgs
    {
        public int Score { get; private set; }

        public ScoreChangedEventArgs(int score)
        {
            this.Score = score;
        }
    }
}