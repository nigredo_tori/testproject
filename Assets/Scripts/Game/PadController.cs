﻿using UnityEngine;
using UnityEngine.Networking;

namespace OvermobileTest.Game
{
    /// <summary>
    /// Управляет движением дощечки
    /// 
    /// Использует Rigidbody2D на объекте для перемещения
    /// Использует Collider2D на объекте для проверки выхода за границы экрана
    /// </summary>
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(Collider2D))]
    public class PadController : MonoBehaviour
    {

        [Tooltip("Камера, ограничивающая движение дощечки. Не должна быть повёрнута.")]
        public Camera boundingCamera;

        [Tooltip("Скорость движения дощечки")]
        public float velocity;

        new private Rigidbody2D rigidbody2D;
        new private Collider2D collider2D;

        void Start()
        {
            rigidbody2D = GetComponent<Rigidbody2D>();
            collider2D = GetComponent<Collider2D>();

            if (!boundingCamera)
            {
                boundingCamera = Camera.main;
            }

            rigidbody2D.isKinematic = true;
        }

        void FixedUpdate()
        {
            UpdateVelocity();

            if (boundingCamera)
            {
                CheckBounds();
            }
        }

        private void CheckBounds()
        {
            // Вычисляем границы экрана
            float halfScreenWidth = boundingCamera.aspect * boundingCamera.orthographicSize;
            float screenCenterX = boundingCamera.transform.position.x;
            float rightScreenBorder = screenCenterX + halfScreenWidth;
            float leftScreenBorder = screenCenterX - halfScreenWidth;

            // Вычисляем положение дощечки
            Bounds bounds = collider2D.bounds;


            // Проверяем столкновение с границами
            float leftDelta = leftScreenBorder - bounds.min.x;
            if (leftDelta >= 0)
            {
                rigidbody2D.position += Vector2.right * leftDelta;

                float vx = rigidbody2D.velocity.x;
                rigidbody2D.velocity = Vector2.right * Mathf.Max(0, vx);
            }

            float rightDelta = bounds.max.x - rightScreenBorder;
            if (rightDelta >= 0)
            {
                rigidbody2D.position -= Vector2.right * rightDelta;
                float vx = rigidbody2D.velocity.x;
                rigidbody2D.velocity = Vector2.right * Mathf.Min(0, vx);
            }
        }

        /// <summary>
        /// Меняет скорость дощечки в зависимости от ввода
        /// </summary>
        /// <returns></returns>
        private void UpdateVelocity()
        {
            bool rightPressed = Input.GetKey(KeyCode.RightArrow);
            bool leftPressed = Input.GetKey(KeyCode.LeftArrow);

            float vx = 0;

            if (rightPressed)
            {
                vx += velocity;
            }
            if (leftPressed)
            {
                vx -= velocity;
            }

            rigidbody2D.velocity = vx * Vector2.right;
        }
    }
}