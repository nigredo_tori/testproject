﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace OvermobileTest.Game
{
    /// <summary>
    /// Реализует жизненный цикла шара (создание, уничтожение).
    /// Оповещает подписчиков при попадании шара на дощечку или при падении на землю.
    /// </summary>
    [RequireComponent(typeof(Renderer))]
    [RequireComponent(typeof(Collider2D))]
    [RequireComponent(typeof(ParticleSystem))]
    public class BallLifetime : NetworkBehaviour
    {

        [Tooltip("Максимальная продолжительность взрыва. После истечения этого времени объект будет удалён")]
        public float blastDuration = 3;

        /// <summary>
        /// Вызывается при попадании шара на дощечку
        /// </summary>
        public event Action<BallLifetime> WasCaught;

        /// <summary>
        /// Вызывается при падении шара на землю
        /// </summary>
        public event Action<BallLifetime> Missed;

        new private Renderer renderer;
        new private Collider2D collider2D;
        new private ParticleSystem particleSystem;

        // Время смерти шара (fixedTime сервера)
        // Либо PositiveInfinity если шар ещё не столкнулся с дощечкой
        [SyncVar]
        private float deathTime;

        // True если уже отобразили смерть (шар взорвался)
        private bool dead;
        
        // Для координации движения с сервером
        private IServerTime serverTime;

        #region Обработчики событий Unity

        void Awake()
        {

            renderer = GetComponent<Renderer>();
            collider2D = GetComponent<Collider2D>();
            particleSystem = GetComponent<ParticleSystem>();

            particleSystem.Stop();
        }

        void Start()
        {
            serverTime = FindObjectOfType<SynchronizedGameState>();

            deathTime = float.PositiveInfinity;

            // На клиенте не должно быть обработки столкновений
            if (isClient)
            {
                collider2D.enabled = false;
            }
        }

        [Server]
        public void OnTriggerEnter2D(Collider2D otherCollider)
        {
            //Столкновение с землёй или дощечкой

            GameObject other = otherCollider.gameObject;

            if (other.CompareTag(Tags.PAD))
            {
                HandleWasCaught();
            }
            else if (other.CompareTag(Tags.EARTH))
            {
                HandleMissed();
            }
        }

        public void FixedUpdate()
        {
            if (isClient
                && !dead
                && deathTime <= serverTime.ServerFixedTime)
            {
                dead = true;
                ShowCaught();
            }
        }

        #endregion

        #region Обработка попадания и промаха

        /// <summary>
        /// Отображает падение шара на землю.
        /// </summary>
        private void HandleMissed()
        {
            OnMissed();
        }

        /// <summary>
        /// Обработка попадания шара в ракетку (на сервере)
        /// </summary>
        private void HandleWasCaught()
        {
            deathTime = Time.fixedTime;
            ShowCaught();
            StartCoroutine(DoDestroy(blastDuration));
            OnWasCaught();
        }

        /// <summary>
        /// Отображает уничтожение шара при попадании на дощечку
        /// </summary>
        private void ShowCaught()
        {
            // Заменяем шар на систему частиц (анимацию взрыва)
            renderer.enabled = false;
            collider2D.enabled = false;
            particleSystem.Play();
        }

        private IEnumerator DoDestroy(float delay)
        {
            yield return new WaitForSeconds(delay);
            NetworkServer.Destroy(gameObject);
        }

        #endregion

        #region Запуск событий

        private void OnMissed()
        {
            if (Missed != null)
            {
                Missed(this);
            }
        }

        private void OnWasCaught()
        {
            if (WasCaught != null)
            {
                WasCaught(this);
            }
        }
        #endregion
    }
}