﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

namespace OvermobileTest.Game
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class BallMovement : NetworkBehaviour
    {

        [Tooltip("Угловая скорость осцилляции")]
        public float omega;

        [Tooltip("Амплитуда осцилляции")]
        public float amplitude;

        [Tooltip("Характерное время исправления нестыковок с позиционированием")]
        public float positionCorrectionPeriod;

        // Вертикальная скорость
        [SyncVar]
        private float velocityY;

        // сдвиг по фазе
        [SyncVar]
        private float phi;

        // Начальная координата по y
        [SyncVar]
        private float startY;

        // Координата оси, вокруг которой происходит осцилляция, по x
        [SyncVar]
        private float centerX;

        // время создания (fixedTime сервера)
        [SyncVar]
        private float creationTime;

        // Максимальное ускорение
        private float maxAccelerationX;

        // Максимальная скорость
        private float maxVelocityX;

        new private Rigidbody2D rigidbody2D;

        // Для координации движения с сервером
        private IServerTime serverTime;

        /// <summary>
        /// Задание начальных данных для шара. Происходит только на сервере - затем данные копируются на клиент.
        /// </summary>
        [Server]
        public void Init(float centerX, float startY, float velocityY)
        {
            this.velocityY = velocityY;
            this.centerX = centerX;
            this.startY = startY;
            phi = 2 * Random.value * Mathf.PI;
            creationTime = Time.fixedTime;
        }

        public void Start()
        {
            rigidbody2D = GetComponent<Rigidbody2D>();
            serverTime = FindObjectOfType<SynchronizedGameState>();

            maxVelocityX = amplitude * omega;
            maxAccelerationX = omega * omega * amplitude;

            UpdateRigidBody();
        }

        void FixedUpdate()
        {
            UpdateRigidBody();
        }

        private void UpdateRigidBody()
        {
            float timeSinceCreation = serverTime.ServerFixedTime - creationTime;

            float alpha = phi + omega * timeSinceCreation;
            float cosAlpha = Mathf.Cos(alpha);
            float sinAlpha = Mathf.Sin(alpha);

            // Рассчёт идеального положения и движения шара для данного момента времени
            float x = centerX + amplitude * sinAlpha;
            float y = startY + velocityY * timeSinceCreation;
            var pos = new Vector2(x, y);

            float vx = maxVelocityX * cosAlpha;
            var v = new Vector2(vx, velocityY);

            float ax = -maxAccelerationX * sinAlpha;
            var f = new Vector2(rigidbody2D.mass * ax, 0);

            // Корректировка нестыковок
            Vector2 deltaPos = pos - rigidbody2D.position;
            v += deltaPos * (Time.fixedDeltaTime / positionCorrectionPeriod);

            // Применение рассчитанных данных
            rigidbody2D.velocity = v;
            rigidbody2D.AddForce(f, ForceMode2D.Force);
        }
    }
}