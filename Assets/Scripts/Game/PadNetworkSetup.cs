﻿using UnityEngine;
using System.Collections;
using OvermobileTest.Game;
using UnityEngine.Networking;

public class PadNetworkSetup : NetworkBehaviour {

    void Start () {
        if (!isServer)
        {
            GetComponent<PadController>().enabled = false;
        }
    }
}
