﻿using System;
using UnityEngine;
using UnityEngine.UI;


namespace OvermobileTest.UI
{
    /// <summary>
    /// Обработчик нажатий кнопок в главном меню.
    /// 
    /// Передаёт полученные нажатия подписчикам.
    /// </summary>
    public class MainMenuController : MonoBehaviour
    {

        [Tooltip("Кнопка \"Войти\"")]
        public Button startButton;

        [Tooltip("Кнопка \"Посмотреть\"")]
        public Button spectateButton;

        [Tooltip("Кнопка \"Выйти\"")]
        public Button exitButton;

        /// <summary>
        /// Выбрана новая игра
        /// </summary>
        public event EventHandler StartSelected;

        /// <summary>
        /// Выбрано подключение к удалённому серверу
        /// </summary>
        public event EventHandler SpectateSelected;

        /// <summary>
        /// Выбран выход из игры
        /// </summary>
        public event EventHandler ExitSelected;

        /// <summary>
        /// Отображает меню на экране
        /// </summary>
        public void Show()
        {
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Убирает меню с экрана
        /// </summary>
        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void OnEnable()
        {
            startButton.onClick.AddListener(OnStartSelected);
            spectateButton.onClick.AddListener(OnSpectateSelected);
            exitButton.onClick.AddListener(OnExitSelected);
        }

        public void OnDisable()
        {
            startButton.onClick.RemoveListener(OnStartSelected);
            spectateButton.onClick.RemoveListener(OnSpectateSelected);
            exitButton.onClick.RemoveListener(OnExitSelected);
        }

        /// <summary>
        /// Выход из приложения
        /// </summary>
        private void OnExitSelected()
        {
            EventHandler handler = ExitSelected;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Просмотр удалённой игры
        /// </summary>
        private void OnSpectateSelected()
        {
            EventHandler handler = SpectateSelected;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Новая игра
        /// </summary>
        private void OnStartSelected()
        {
            EventHandler handler = StartSelected;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }
    }
}