﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;


namespace OvermobileTest.UI
{
    /// <summary>
    /// Экран поражения.
    /// 
    /// Останавливает игру (при помощи Time.timeScale) при активации
    /// </summary>
    public class GameOverScreen : MonoBehaviour
    {

        [Tooltip("Кнопка \"В главное меню\"")]
        public Button exitButton;

        /// <summary>
        /// Отображает экран поражения на экране
        /// </summary>
        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void OnEnable()
        {
            exitButton.onClick.AddListener(OnExitPressed);

            // Когда экран отображается - игра останавливается
            Time.timeScale = 0;
        }

        public void OnDisable()
        {
            exitButton.onClick.RemoveListener(OnExitPressed);
            
            Time.timeScale = 1;
        }

        /// <summary>
        /// Выход из приложения
        /// </summary>
        private void OnExitPressed()
        {
            FindObjectOfType<NetworkManager>().StopServer();
            Application.LoadLevel("Main Menu");
        }
    }
}