﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

namespace OvermobileTest.UI
{
    /// <summary>
    /// Настраивает сцену главного меню.
    /// </summary>
    public class MenuSetup : MonoBehaviour
    {
        public MainMenuController mainMenuController;
        public ConnectionMenuController connectionMenuController;

        private MainMenuModel mainMenuModel;

        void Awake()
        {

            NetworkManager networkManager = FindObjectOfType<NetworkManager>();
            mainMenuModel = new MainMenuModel(networkManager);

            mainMenuController.StartSelected += (sender, args) => mainMenuModel.NewGame();
            mainMenuController.ExitSelected += (sender, args) => mainMenuModel.ExitGame();
            mainMenuController.SpectateSelected += MainMenuController_SpectateSelected;

            connectionMenuController.ConnectSelected += ConnectionMenuController_ConnectSelected;
            connectionMenuController.BackSelected += ConnectionMenuController_BackSelected;

            mainMenuModel.ClientConnectionFailed += MainMenuModel_ClientConnectionFailed;

            mainMenuController.Show();
            connectionMenuController.Hide();
        }

        private void MainMenuModel_ClientConnectionFailed(object sender, System.EventArgs e)
        {
            connectionMenuController.Show();
        }

        private void ConnectionMenuController_BackSelected(object sender, System.EventArgs e)
        {
            mainMenuController.Show();
            connectionMenuController.Hide();
        }

        private void ConnectionMenuController_ConnectSelected(object sender, ConnectEventArgs args)
        {
            connectionMenuController.Hide();
            string address = args.Address;
            mainMenuModel.Spectate(address);
        }

        private void MainMenuController_SpectateSelected(object sender, System.EventArgs e)
        {
            mainMenuController.Hide();
            connectionMenuController.Show();
        }
    }
}