﻿using System;
using UnityEngine;
using UnityEngine.UI;


namespace OvermobileTest.UI
{
    /// <summary>
    /// Обработчик нажатий кнопок в главном меню.
    /// 
    /// Передаёт полученные нажатия подписчикам.
    /// </summary>
    public class ConnectionMenuController : MonoBehaviour
    {
        [Tooltip("Поле ввода адреса сервера")]
        public InputField addressField;

        [Tooltip("Кнопка \"Подключиться\"")]
        public Button connectButton;

        [Tooltip("Кнопка \"Назад\"")]
        public Button backButton;

        /// <summary>
        /// Выбрано подключение к удалённому серверу
        /// </summary>
        public event EventHandler<ConnectEventArgs> ConnectSelected;

        /// <summary>
        /// Выбран возврат в предыдущее меню
        /// </summary>
        public event EventHandler BackSelected;
        
        /// <summary>
        /// Отображает меню на экране
        /// </summary>
        public void Show()
        {
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Убирает меню с экрана
        /// </summary>
        public void Hide()
        {
            gameObject.SetActive(false);
        }

        /// <summary>
        /// Введённый адрес сервера
        /// </summary>
        private string Address
        {
            get
            {
                return addressField.text;
            }
        }

        public void OnEnable()
        {
            connectButton.onClick.AddListener(OnConnectSelected);
            backButton.onClick.AddListener(OnBackSelected);
        }

        public void OnDisable()
        {
            connectButton.onClick.RemoveListener(OnConnectSelected);
            backButton.onClick.RemoveListener(OnBackSelected);
        }

        /// <summary>
        /// Возврат в предыдущее меню
        /// </summary>
        private void OnBackSelected()
        {
            EventHandler handler = BackSelected;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Подключение к серверу
        /// </summary>
        private void OnConnectSelected()
        {
            EventHandler<ConnectEventArgs> handler = ConnectSelected;
            if (handler != null)
            {
                var args = new ConnectEventArgs(Address);
                handler(this, args);
            }
        }
    }
    
    public class ConnectEventArgs : EventArgs
    {
        private readonly string address;

        public ConnectEventArgs(String address)
        {
            this.address = address;
        }

        public string Address
        {
            get
            {
                return address;
            }
        }
    }
}