﻿using UnityEngine;
using UnityEngine.UI;

namespace OvermobileTest.Game
{
    /// <summary>
    /// Отображает набранные очки.
    /// </summary>
    public class ScoreUI : MonoBehaviour
    {
        private Text label;
        
        public void Init(IGameState gameState)
        {
            this.label = GameObject.FindGameObjectWithTag(Tags.SCORE_LABEL).GetComponent<Text>();

            gameState.ScoreChanged += GameState_ScoreChanged;

            UpdateLabel(gameState.Score);
        }

        private void GameState_ScoreChanged(object sender, ScoreChangedEventArgs e)
        {
            UpdateLabel(e.Score);
        }

        private void UpdateLabel(int score)
        {
            label.text = score.ToString();
        }
    }
}