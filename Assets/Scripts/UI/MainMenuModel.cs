﻿using UnityEngine;
using UnityEngine.Networking;
using System;

namespace OvermobileTest.UI
{
    /// <summary>
    /// Реализует действия, выполняемые главным меню
    /// </summary>
    public class MainMenuModel
    {

        /// <summary>
        /// Не удалось подключиться к серверу
        /// </summary>
        public event EventHandler ClientConnectionFailed;

        private NetworkManager networkManager;

        /// <summary>
        /// Выход из приложения
        /// </summary>
        public void ExitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        public MainMenuModel(NetworkManager networkManager)
        {
            this.networkManager = networkManager;
        }

        /// <summary>
        /// Начало новой игры (в режиме сервера)
        /// </summary>
        public void NewGame()
        {
            networkManager.StartServer();
        }

        /// <summary>
        /// Наблюдение за игрой на удалённом сервере
        /// </summary>
        /// <param name="address"></param>
        public void Spectate(string address)
        {
            var networkManager = UnityEngine.Object.FindObjectOfType<NetworkManager>();
            networkManager.networkAddress = address;
            networkManager.StartClient();
        }
    }
}
