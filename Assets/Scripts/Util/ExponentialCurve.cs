﻿using UnityEngine;
using System;

namespace OvermobileTest.Util
{
    /// <summary>
    /// Позволяет настроить кривую зависимости величины от параметра
    /// (например, скорости от времени)
    /// </summary>
    [Serializable]
    public class ExponentialCurve
    {

        [Tooltip("Значение при t = 0")]
        public float startValue;

        [Tooltip("Значение, к которому сходится кривая при t -> +infinity")]
        public float finishValue;

        [Tooltip("Значение t, при котором кривая находится ровно посередине между начальным и конечным значением")]
        public float halflife;

        /// <summary>
        /// Получить значение зависимой величины в зависимости от параметра
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public float GetValue(float t)
        {
            return finishValue + (startValue - finishValue) * Mathf.Pow(2, -t / halflife);
        }
    }
}