﻿namespace OvermobileTest
{
    /// <summary>
    /// Используемые в коде теги
    /// </summary>
    public static class Tags
    {

        /// <summary>
        /// Дощечка
        /// </summary>
        public static string PAD = "Pad";

        /// <summary>
        /// Земля
        /// </summary>
        public static string EARTH = "Earth";

        /// <summary>
        /// Элемент интерфейса, отображающий текущий счёт
        /// </summary>
        public static string SCORE_LABEL = "Score Label";
    }
}